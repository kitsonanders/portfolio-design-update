import React from 'react'

import '../css/bottom.css'
import Image from '../components/image'
import ReactImage from '../components/reactImage'
import { IconContext } from 'react-icons'
import { FaPaperPlane } from 'react-icons/fa'

const Bottom = () => (
  <IconContext.Provider
    value={{
      style: {
        width: '100%',
        height: '100%',
        color: '#e4e7eb',
        background: '#486581',
        padding: '12px 13px 12px 10px',
        borderRadius: '12px',
        border: 'none',
        boxShadow: '2px 3px 5px hsla(0, 0%, 0%, 0.9), inset 2px 2px 0 #A8A8A8',
      },
    }}
  >
    <div className="bottomDiv">
      <div id="graphqlImg">
        <Image />
      </div>
      <form
        name="contact"
        method="post"
        data-netlify="true"
        data-netlify-honeypot="bot-field"
      >
        <div id="formContainer">
          <p id="bottomEmailAddress">Charles.Beadle@protonmail.com</p>
          <input
            type="hidden"
            name="form-name"
            value="contact"
            placeholder="Don’t fill this out if you're human"
          />
          <input name="name" type="text" placeholder="*Name" required />
          <input name="email" type="email" placeholder="*Email" required />
          <input name="company" type="text" placeholder="*Company" required />
          <textarea rows="5" name="message" placeholder="*Message" required />

          <br />
          <div id="buttonContainer">
            <p id="formText">Message me</p>
            <button type="submit" id="leButton">
              <FaPaperPlane />
            </button>
          </div>
        </div>
      </form>
      <div id="reactImg">
        <ReactImage />
      </div>
    </div>
  </IconContext.Provider>
)

export default Bottom
