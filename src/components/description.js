import React from 'react'

import '../css/description.css'
import ImageMe from './imageMe'

const Description = () => (
  <div className="descriptionDiv">
    <div id="imageMeContainer">
      <ImageMe />
    </div>
  </div>
)

export default Description
