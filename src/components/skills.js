import React from 'react'

import '../css/skills.css'
import { FaLink } from 'react-icons/fa'

const Skills = () => (
  <div id="skillsWrapper">
    <div id="skillsDivParaContainer">
      <div id="skillsExperience">
        <h2 className="skillsHeading">I'm experienced with:</h2>
        <p>HTML</p>
        <p>CSS</p>
        <p>Javascript</p>
        <p>React</p>
        <p>Gatsby</p>
        <p>Responsive Design</p>
      </div>
      <div id="skillsLearning">
        <h2 className="skillsHeading">Learning:</h2>
        <p>Advanced React & GraphQL</p>
        <p>Headless WordPress with Next.js & Gatsby</p>
        <p>Apollo Client with React</p>
      </div>
      <div id="skillsOther">
        <h2 className="skillsHeading">Other skills:</h2>
        <p>Version Control/Git</p>
        <p>Dev Tools</p>
        <p>Command-line</p>
        <p>Linux/Windows/Mac</p>
        <p>Netlify</p>
        <p>Manage DNS</p>
        <p>Deploy to host</p>
      </div>
      <div id="skillsProject">
        <h2 className="skillsHeading">Side project that I made:</h2>
        <a
          href="https://www.charlesbeadle.tech"
          target="_blank"
          rel="noopener noreferrer"
        >
          <FaLink />
          <span>CharlesBeadle.tech</span>
        </a>
      </div>
      <div id="skillsEducation">
        <h2 className="skillsHeading">Education:</h2>
        <p>B.A. FIU</p>
        <p>Political Science</p>
      </div>
    </div>
  </div>
)

export default Skills
